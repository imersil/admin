import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:nuskin_warranty_ph/app/constants/app_fonts.dart';
import 'package:nuskin_warranty_ph/app/constants/app_images.dart';
import 'package:nuskin_warranty_ph/app/constants/app_string.dart';
import 'package:nuskin_warranty_ph/app/global_controller/modal_scan_controller.dart';
import 'package:nuskin_warranty_ph/app/global_controller/select_modal_controller.dart';
import 'package:nuskin_warranty_ph/app/global_controller/support_modal_controller.dart';
import 'package:nuskin_warranty_ph/app/global_styles/global_styles.dart';
import 'package:nuskin_warranty_ph/app/global_widgets/text_field_modal.dart';
import 'package:nuskin_warranty_ph/app/global_widgets/title_description.dart';
import 'package:nuskin_warranty_ph/app/models/city_model.dart';
import 'package:nuskin_warranty_ph/app/modules/scan_code/widgets/overlay.dart';
import 'package:photo_view/photo_view.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Utils {
  static void showLoadingIndicator({String? message}) {
    Get.dialog(
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              SizedBox(
                height: 10,
              ),
              Text(
                message ?? CommonString.LOADING,
                style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    decoration: TextDecoration.none),
              )
            ],
          ),
        ),
        barrierDismissible: false);
  }

  static void closeLoadingIndicator() {
    Get.back();
  }

  static Future<dynamic> handleError(
      [String? title, String? message, String? buttonText]) {
    return showDialog(
      context: Get.context!,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title!),
        content: Text(message!),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text(buttonText ?? CommonString.CANCEL),
            onPressed: () => Get.back(),
          )
        ],
      ),
      barrierDismissible: false,
    );
  }

  static Future? handleNavigation(String namePage, id,
      {removeState = "to", argument}) {
    switch (namePage) {
      case '/report-device':
        switch (removeState) {
          case "to":
            return Get.toNamed(namePage, id: id, arguments: argument ?? null);
          case "off":
            return Get.offNamed(namePage, id: id, arguments: argument ?? null);
          case "offAll":
            return Get.offAllNamed(namePage,
                id: id, arguments: argument ?? null);
        }
        break;

      case '/detail-status-device':
        switch (removeState) {
          case "to":
            return Get.toNamed(namePage, id: id, arguments: argument ?? null);
          case "off":
            return Get.offNamed(namePage, id: id, arguments: argument ?? null);
          case "offAll":
            return Get.offAllNamed(namePage,
                id: id, arguments: argument ?? null);
        }
        break;
      case '/handle-warranty-status':
        switch (removeState) {
          case "to":
            return Get.toNamed(namePage, id: id, arguments: argument ?? null);
          case "off":
            return Get.offNamed(namePage, id: id, arguments: argument ?? null);
          case "offAll":
            return Get.offAllNamed(namePage,
                id: id, arguments: argument ?? null);
        }
        break;
      case '/warranty-error-status':
        switch (removeState) {
          case "to":
            return Get.toNamed(namePage, id: id, arguments: argument ?? null);
          case "off":
            return Get.offNamed(namePage, id: id, arguments: argument ?? null);
          case "offAll":
            return Get.offAllNamed(namePage,
                id: id, arguments: argument ?? null);
        }
    }
  }

  static Future<dynamic> showErrorModal(message,
      {String? cancelText, String? icon, String? title, bool isTitle = true}) {
    return Get.defaultDialog(
        barrierDismissible: false,
        title: "",
        radius: 10,
        actions: [
          Align(
            alignment: Alignment.topRight,
            child: CupertinoButton(
              onPressed: () {
                Get.back();
              },
              child: isTitle == true
                  ? Text(
                      cancelText ?? CommonString.OK,
                      style: TextStyle(
                          fontSize: 17,
                          color: Color(0xff00B4E7),
                          fontWeight: FontWeight.bold),
                    )
                  : Center(
                      child: Text(
                        cancelText ?? CommonString.OK,
                        style: TextStyle(
                            fontSize: 17,
                            color: Color(0xff00B4E7),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
            ),
          )
        ],
        content: Builder(
          builder: (context) {
            var width = MediaQuery.of(context).size.width;
            return Container(
              width: width,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: [
                    Image.asset(
                      icon ?? AppImages.icRoundedClose,
                      width: 35,
                      height: 35,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Visibility(
                        visible: isTitle == true,
                        child: Text(
                          title ?? CommonString.ERROR_CODE_NOT_DETECTED,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 19,
                              color: Color(0xff747474),
                              fontWeight: FontWeight.bold),
                        )),
                    Padding(
                      padding: isTitle == false
                          ? EdgeInsets.symmetric(horizontal: 25)
                          : EdgeInsets.all(0.0),
                      child: Text(
                        message,
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 19, color: Color(0xff747474)),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }

  static Future<dynamic> showErrorModalOptions(message,
      {String? confirm, String? cancel, String? icon, String? title}) {
    return Get.defaultDialog(
        barrierDismissible: false,
        title: "",
        radius: 10,
        actions: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: CupertinoButton(
                    onPressed: () {
                      Get.back(result: true);
                    },
                    child: Text(
                      confirm ?? CommonString.YES,
                      style: TextStyle(
                          fontSize: 17,
                          color: Color(0xff00B4E7),
                          fontWeight: FontWeight.bold),
                    )),
              ),
              CupertinoButton(
                  onPressed: () {
                    Get.back(result: false);
                  },
                  child: Text(
                    cancel ?? CommonString.NO,
                    style: TextStyle(
                        fontSize: 17,
                        color: Color(0xff00B4E7),
                        fontWeight: FontWeight.bold),
                  ))
            ],
          )
        ],
        content: Builder(
          builder: (context) {
            var width = MediaQuery.of(context).size.width;
            return Container(
              width: width,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: [
                    Image.asset(
                      icon ?? AppImages.icRoundedClose,
                      width: 35,
                      height: 35,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      title ?? CommonString.ERROR_CODE_NOT_DETECTED,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 19,
                          color: Color(0xff747474),
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      message,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 19, color: Color(0xff747474)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }

  static Future<dynamic> showSuccessModal(
      {String? titlePart1,
      String? message,
      String? titlePart2,
      String? cancelText}) {
    return Get.defaultDialog(
        barrierDismissible: false,
        title: "",
        radius: 10,
        actions: [
          Align(
            alignment: Alignment.topRight,
            child: CupertinoButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                cancelText ?? CommonString.OK,
                style: TextStyle(
                    fontSize: 17,
                    color: Color(0xff00B4E7),
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
        content: Builder(builder: (context) {
          var width = MediaQuery.of(context).size.width;

          return Container(
            width: width,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  Image.asset(
                    AppImages.icRoundedTick,
                    width: 35,
                    height: 35,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    titlePart1 ?? CommonString.SUCCESS_TITLE_PART_1,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 19,
                      color: Color(0xff747474),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    message!,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 35, color: Color(0xff747474)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    titlePart2 ?? CommonString.SUCCESS_TITLE_PART_2,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 19, color: Color(0xff747474)),
                  ),
                ],
              ),
            ),
          );
        }));
  }

  static Future<dynamic> showSuccessModalCustom(
      {String? titlePart1,
      String? message,
      String? titlePart2,
      String? titlePart3,
      String? cancelText}) {
    return Get.defaultDialog(
        barrierDismissible: false,
        title: "",
        radius: 10,
        actions: [
          Align(
            alignment: Alignment.topRight,
            child: CupertinoButton(
              onPressed: () {
                Get.back();
              },
              child: Text(
                cancelText ?? CommonString.OK,
                style: TextStyle(
                    fontSize: 17,
                    color: Color(0xff00B4E7),
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
        content: Builder(builder: (context) {
          var width = MediaQuery.of(context).size.width;

          return Container(
            width: width,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  Image.asset(
                    AppImages.icRoundedTick,
                    width: 35,
                    height: 35,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    titlePart1 ?? CommonString.SUCCESS_TITLE_PART_1,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        color: Color(0xff707070),
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    message!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xff707070),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text.rich(TextSpan(style: TextStyle(fontSize: 16), children: [
                    TextSpan(
                        text: titlePart2! + " ",
                        style: TextStyle(color: Color(0xff707070))),
                    TextSpan(
                      text: titlePart3,
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Color(0xffE4238F)),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Get.back(result: true);
                        },
                    ),
                  ]))
                  // Text(
                  //   titlePart2 ?? CommonString.SUCCESS_TITLE_PART_2,
                  //   textAlign: TextAlign.center,
                  //   style: TextStyle(fontSize: 19, color: Color(0xff747474)),
                  // ),
                ],
              ),
            ),
          );
        }));
  }

  static Future<dynamic> showModalScanner(
      {String? appbarTitle, String? titleCenter}) {
    return Get.generalDialog(
      barrierColor: Colors.white,
      // Background color
      barrierDismissible: false,
      // transitionDuration: Duration(milliseconds: 400),
      // How long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        // Makes widget fullscreen
        return SizedBox.expand(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(0xff00B4E7),
              centerTitle: false,
              title: Text(
                appbarTitle!,
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 24,
                    fontFamily: AppFonts.verLag),
              ),
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () => Get.back(),
              ),
            ),
            body: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                new Container(
                  child: new Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: new Center(
                      child: QrCamera(
                        onError: (context, error) => Text(
                          error.toString(),
                          style: TextStyle(color: Colors.red),
                        ),
                        qrCodeCallback: (code) {
                          if (code!.isNotEmpty) {
                            final serialNumber = code;
                            Get.back(result: serialNumber);
                          }
                        },
                      ),
                    ),
                  ),
                ),
                Center(child: GetBuilder<ModalScanController>(builder: (_) {
                  return Stack(
                    children: <Widget>[
                      SizedBox(
                        height: 300.0,
                        width: 300.0,
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.transparent, width: 2.0)),
                        ),
                      ),
                      Positioned(
                        bottom: _.animationScanner,
                        child: Container(
                            width: 300.0,
                            height: 24.0,
                            decoration: new BoxDecoration(
                                gradient: new LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    stops: [0.0, 1.0],
                                    colors: [_.firstColor, _.secondColor]))),
                      )
                    ],
                  );
                })),
                Container(
                    constraints: const BoxConstraints.expand(),
                    decoration: ShapeDecoration(
                      shape: QrScannerOverlayShape(
                          largeBorderColor: Colors.white,
                          cutOutSize: 300.0,
                          borderColor: Colors.blue),
                    ),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: TitleDescription(
                        text: titleCenter ?? "",
                        color: Color(0xffFCFCFC),
                      ),
                    )),
              ],
            ),
          ),
        );
      },
    );
  }

  static Future<dynamic> showImageModal(
      {String? appbarTitle,
      required List<Map<String, String>> imageList,
      required int index}) {
    return Get.generalDialog(
      barrierColor: Colors.white,
      // Background color
      barrierDismissible: false,
      // transitionDuration: Duration(milliseconds: 400),
      // How long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        // Makes widget fullscreen
        return SizedBox.expand(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Color(0xff00B4E7),
                centerTitle: false,
                title: Text(
                  appbarTitle!,
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 24,
                      fontFamily: AppFonts.verLag),
                ),
                leading: IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () => Get.back(),
                ),
              ),
              body: PageView.builder(
                  itemCount: imageList.length,
                  controller: PageController(initialPage: index),
                  itemBuilder: (context, index) {
                    return PhotoView(
                      imageProvider:
                          NetworkImage(imageList[index]['imageUrl']!),
                    );
                  })),
        );
      },
    );
  }

  static Color fromHex(String? hexString) {
    final buffer = StringBuffer();
    if (hexString!.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static Future<dynamic> selectFile(
      {String? title, String? imageText, String? videoText}) async {
    return showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(title ?? AppString.SELECT_ATTACHMENTS_FILE),
            backgroundColor: Colors.white,
            children: [
              SimpleDialogOption(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text(
                    imageText ?? AppString.UPLOAD_IMAGE,
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                onPressed: () {
                  Get.back(result: "image");
                },
              ),
              SimpleDialogOption(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text(
                    videoText ?? AppString.UPLOAD_VIDEO,
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                onPressed: () {
                  Get.back(result: "video");
                },
              )
            ],
          );
        });
  }

  static checkExpiredWarranty(deviceData) {
    if (deviceData.invoiceDate != null && deviceData.warrantyDay != null) {
      if (deviceData.invoiceDate == "0000-00-00") {
        return 1;
      }
      var purchaseFormat = DateFormat('yyyy-MM-DD');

      var purchaseDate = purchaseFormat.parse(deviceData.invoiceDate!);
      var warrantyDate = purchaseDate
          .add(Duration(days: int.parse(deviceData.warrantyDay, radix: 10)));
      var warrantyDayLeft = ((warrantyDate.millisecondsSinceEpoch -
                  DateTime.now().millisecondsSinceEpoch) /
              (24 * 60 * 60 * 1000))
          .ceil();
      return warrantyDayLeft;
    }
    return 0;
  }

  static String getWarrantyValidity(
      String? invoiceDate, String? warrantyDate, String emptyInvoiceDate) {
    if (invoiceDate != null) {
      if (invoiceDate == "0000-00-00") {
        return emptyInvoiceDate;
      }
      if (warrantyDate != null) {
        var purchaseFormat = DateFormat('yyyy-MM-DD');

        var purchaseDate = purchaseFormat.parse(invoiceDate);
        var warrantyDay = purchaseDate
            .add(Duration(days: int.parse(warrantyDate, radix: 10)));
        String warrantyValidity = DateFormat('ddMMMyyyy').format(warrantyDay);
        return warrantyValidity;
      }
    }

    return "";
  }

  static formatDate(date) {
    var inputFormat = DateFormat('yyyy-MM-dd');
    var inputDate = inputFormat.parse(date);
    var outputFormat = DateFormat('MM/dd/yy');
    var outputDate = outputFormat.format(inputDate); // <-- Format date
    return outputDate;
  }

  static String formatBytes(int bytes, int decimals) {
    if (bytes <= 0) return "0";
    return ((bytes / pow(1000, 2)).toStringAsFixed(decimals));
  }

  static Future<void> setFcmId(String fcmId) async {
    var pref = await SharedPreferences.getInstance();
    await pref.setString(fcmId, fcmId);
  }

  static Future<String?> getFcmId(String fcmId) async {
    return (await SharedPreferences.getInstance()).getString(fcmId) ?? null;
  }

  static Future<dynamic> showSuccess(
      {required String title, String? message, String? buttonText}) {
    return showDialog(
      context: Get.context!,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(message ?? ""),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text(buttonText ?? CommonString.OK),
            onPressed: () => Get.back(),
          )
        ],
      ),
      barrierDismissible: false,
    );
  }

  static Future<dynamic> showSupportModal([Map<String, dynamic>? data]) {
    return showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      context: Get.context!,
      pageBuilder: (_, __, ___) {
        return AnimatedContainer(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(Get.context!).viewInsets.bottom,
          ),
          duration: const Duration(milliseconds: 150),
          child: Center(
            child: Container(
              height: MediaQuery.of(Get.context!).size.height / 1.4,
              margin: const EdgeInsets.only(left: 15, right: 15),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(2))),
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: GetBuilder<SupportModalController>(
                  builder: (controller) {
                    controller.initView(data!);
                    return SingleChildScrollView(
                      child: Padding(
                        padding: GlobalStyles.paddingPageLeftRight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GlobalStyles.sizedBoxHeight,
                            Align(
                              alignment: Alignment.topCenter,
                              child: Text(
                                CommonString.SUPPORT_TITLE,
                                style: TextStyle(
                                    color: Color(0xffe30b7a),
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            InputSupportModalWidget(
                                title: CommonString.FROM,
                                hintText: CommonString.FROM,
                                background: data['isLogged'] == true
                                    ? Color(0xffDBDBDB)
                                    : Colors.white,
                                isEnabled:
                                    data['isLogged'] == true ? false : true,
                                textController: controller.fromController),
                            InputSupportModalWidget(
                              title: CommonString.TO,
                              hintText: CommonString.TO,
                              textController: controller.toController,
                              background: data['isLogged'] == false
                                  ? Color(0xffDBDBDB)
                                  : Colors.white,
                              isEnabled:
                                  data['isLogged'] == true ? true : false,
                            ),
                            InputSupportModalWidget(
                              title: CommonString.SEND_NAME,
                              hintText: CommonString.SEND_NAME,
                              textController: controller.nameController,
                            ),
                            InputSupportModalWidget(
                              title: CommonString.TITLE_MAIL,
                              hintText: CommonString.TITLE_MAIL,
                              textController: controller.titleController,
                            ),
                            InputSupportModalWidget(
                              title: CommonString.CONTENT_MAIL,
                              hintText: CommonString.CONTENT_MAIL,
                              textController: controller.contentController,
                              inputType: TextInputType.multiline,
                              inputAction: TextInputAction.newline,
                              height: 100,
                              maxLine: 6,
                              minLine: 6,
                              textAlign: TextAlign.start,
                            ),
                            GlobalStyles.sizedBoxHeight,
                            controller.imageDisplay.isNotEmpty
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 5),
                                        child: Text(
                                          CommonString.ATTACH_FILE,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(bottom: 15),
                                        height: 150,
                                        child:
                                            GetBuilder<SupportModalController>(
                                          builder: (supportController) =>
                                              ListView.builder(
                                                  shrinkWrap: true,
                                                  itemCount: supportController
                                                      .imageDisplay.length,
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemBuilder:
                                                      (context, index) {
                                                    final item =
                                                        supportController
                                                                .imageDisplay[
                                                            index];
                                                    return Container(
                                                      width: 80,
                                                      margin: EdgeInsets.only(
                                                          right: 15),
                                                      child: Image.file(
                                                        item,
                                                        width: 80,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    );
                                                  }),
                                        ),
                                      )
                                    ],
                                  )
                                : Container(),
                            Padding(
                              padding: EdgeInsets.only(bottom: 15),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      controller.selectImage();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: AssetImage(AppImages.bgButton),
                                          fit: BoxFit.fill,
                                          repeat: ImageRepeat.noRepeat,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(15),
                                        child: Text(
                                          CommonString.ATTACH_FILE,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      controller.submit();
                                    },
                                    child: Container(
                                      width: 100,
                                      margin:
                                          EdgeInsets.only(right: 5, left: 5),
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: AssetImage(AppImages.bgButton),
                                          fit: BoxFit.fill,
                                          repeat: ImageRepeat.noRepeat,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(15),
                                        child: Text(
                                          CommonString.SEND,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: AssetImage(AppImages.bgButton),
                                          fit: BoxFit.fill,
                                          repeat: ImageRepeat.noRepeat,
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(15),
                                        child: Text(
                                          CommonString.CANCEL,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  static Future<dynamic> showSelectModal(
      List<Map<String, dynamic>> provinceList,
      Map<String, dynamic> selectedItem) {
    return showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 300),
      context: Get.context!,
      pageBuilder: (_, __, ___) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 150),
          child: Center(
            child: Container(
              height: MediaQuery.of(Get.context!).size.height / 1.2,
              margin: const EdgeInsets.only(left: 25, right: 25),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(2))),
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: GetBuilder<SelectModalController>(
                    init: SelectModalController(
                        radioList: provinceList, selectedItem: selectedItem),
                    builder: (selectController) {
                      return ListView.builder(
                          itemCount: selectController.radioList.length,
                          itemBuilder: (context, index) {
                            final item = provinceList[index];
                            return InkWell(
                              onTap: () {
                                selectController.onChangeSelect(index);
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    AnimatedCrossFade(
                                      duration: Duration(milliseconds: 300),
                                      firstChild: Image.asset(
                                        AppImages.ic_cb_active,
                                        width: 20,
                                        height: 20,
                                      ),
                                      secondChild: Image.asset(
                                        AppImages.ic_cb_disable,
                                        width: 20,
                                        height: 20,
                                      ),
                                      crossFadeState: item['isSelect'] == true
                                          ? CrossFadeState.showFirst
                                          : CrossFadeState.showSecond,
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(child: Text(item['text']))
                                  ],
                                ),
                              ),
                            );
                          });
                    }),
              ),
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  static Future<dynamic> showConfirmDialog() {
    return showDialog(
      context: Get.context!,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(CommonString.CONFIRM_TITLE),
        content: Text(CommonString.DELETE_MESSAGE),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text(CommonString.OK),
            onPressed: () => Get.back(result: true),
          ),
          CupertinoDialogAction(
            child: Text(CommonString.E_CANCEL),
            onPressed: () => Get.back(result: false),
          )
        ],
      ),
      barrierDismissible: false,
    );
  }

  static formatDataForCustomModal(List<CityModel> provinceList) {
    final listProvince = provinceList
        .map((item) => {'text': item.name, 'code': item.id, "isSelect": false})
        .toList();

    return listProvince;
  }
}

class InputSupportModalWidget extends StatelessWidget {
  const InputSupportModalWidget(
      {Key? key,
      required this.textController,
      this.title,
      this.hintText,
      this.background,
      this.isEnabled = true,
      this.height = 35,
      this.inputType = TextInputType.text,
      this.maxLine = 1,
      this.minLine = 1,
      this.inputAction = TextInputAction.next,
      this.textAlign = TextAlign.center})
      : super(key: key);

  final TextEditingController textController;
  final title;
  final hintText;
  final Color? background;
  final bool? isEnabled;
  final double? height;
  final TextInputType? inputType;
  final maxLine;
  final minLine;

  final TextInputAction? inputAction;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
        ),
        GlobalStyles.sizedBoxHeight_5,
        Container(
          height: height,
          color: background ?? Colors.white,
          child: TextFieldModal(
            hintText: hintText,
            controller: textController,
            isEnabled: isEnabled,
            inputType: inputType,
            maxLines: maxLine,
            minLines: minLine,
            inputAction: inputAction,
            textAlign: textAlign!,
          ),
        )
      ],
    );
  }
}
