class AppImages {
  AppImages._(); //constructor
  //example: AppImages.BackgroundLogo(this.img1,this.img2) khi gọi AppImages.BackgroundLogo bắt buộc truyền 2 biến, tương tự như function php

  static final List defaultSlide = [
    'assets/images/items/bg3.png',
    'assets/images/items/bg4.png'
  ];
  static final String bgDashboard = "assets/images/backgrounds/bgDashboard.png";
  static final String logoMain = "assets/images/logo/logo8.png";
  static final String faviconMain = "assets/images/icons/favicon3.png";
  static final String slideImg1 = "assets/images/items/bg1.png";
  static final String slideImg2 = "assets/images/items/bg2.png";
  static final String slideImg3 = "assets/images/items/image_1.jpg";
  static final String slideImg4 = "assets/images/items/image_2.png";
}
